﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ZwinneTodo.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Items",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Title = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Items", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "Items",
                columns: new[] { "Id", "CreatedAt", "Description", "Title" },
                values: new object[,]
                {
                    { 1, new DateTime(2019, 4, 8, 0, 0, 0, 0, DateTimeKind.Unspecified), "Long description of item Test 1", "Test 1" },
                    { 2, new DateTime(2019, 4, 9, 0, 0, 0, 0, DateTimeKind.Unspecified), "Long description of item Test 2", "Test 2" },
                    { 3, new DateTime(2019, 4, 10, 0, 0, 0, 0, DateTimeKind.Unspecified), "Long description of item Test 3", "Test 3" },
                    { 4, new DateTime(2019, 4, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), "Long description of item Test 4", "Test 4" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Items");
        }
    }
}
