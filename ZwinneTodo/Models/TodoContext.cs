﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ZwinneTodo.Models
{
    public class TodoContext : DbContext
    {
        public TodoContext(DbContextOptions<TodoContext> options) : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<TodoItem>().HasData(
                new TodoItem() { Id = 1, Title = "Test 1", Description = "Long description of item Test 1", CreatedAt = new DateTime(2019, 4, 11) - TimeSpan.FromDays(3) },
                new TodoItem() { Id = 2, Title = "Test 2", Description = "Long description of item Test 2", CreatedAt = new DateTime(2019, 4, 11) - TimeSpan.FromDays(2) },
                new TodoItem() { Id = 3, Title = "Test 3", Description = "Long description of item Test 3", CreatedAt = new DateTime(2019, 4, 11) - TimeSpan.FromDays(1) },
                new TodoItem() { Id = 4, Title = "Test 4", Description = "Long description of item Test 4", CreatedAt = new DateTime(2019, 4, 11) }
                );
        }

        public DbSet<TodoItem> Items { get; set; }
    }
}
