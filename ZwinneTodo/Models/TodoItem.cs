using System;

namespace ZwinneTodo.Models
{
    public class TodoItem 
    {
        public int Id {get;set;}
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}