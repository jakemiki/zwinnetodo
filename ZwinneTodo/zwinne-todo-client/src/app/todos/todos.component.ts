import { Component, OnInit } from '@angular/core';
import { TodoService } from './todo.service';
import { TodoItem } from './todo.models';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.scss']
})
export class TodosComponent implements OnInit {
  items: TodoItem[];

  constructor(private todoService: TodoService) { }

  ngOnInit() {
    this.getItems();
  }

  getItems() {
    this.todoService.getAll().subscribe((items) => {
      this.items = items;
    });
  }

  delete(id: number) {
    this.todoService.delete(id).subscribe(() => {
      this.getItems();
    })
  }

}
