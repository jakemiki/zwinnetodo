import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TodoService } from '../todo.service';
import { TodoItem } from '../todo.models';

@Component({
  selector: 'app-todos-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class TodosViewComponent implements OnInit {

  item: TodoItem;

  constructor(private activatedRoute: ActivatedRoute, private todoService: TodoService, private router: Router) { }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(params => {
      const id = params.get('id');
      if (id && !Number.isNaN(+id)) {
        this.todoService.getOne(+id).subscribe(item => {
          this.item = item;
        });
      } else {
        this.router.navigate(['']);
      }
    });
  }

}
